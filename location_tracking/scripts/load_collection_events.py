import django
import csv
from datetime import datetime


def main():

    with open('../fixtures/collection_records.csv', mode='r') as file:
        csv_file = csv.reader(file)

        for line in list(csv_file)[100:]:
            print(line)
            pit_tag_code = line[0]
            fish = Fish.objects.get(pit_tag_code=pit_tag_code)
            collection_location, _ = CollectionLocation.objects.get_or_create(name=line[1].strip())
            detection_time = datetime.strptime(line[2], '%m/%d/%Y %H:%M')
            antenna_name = line[3]

            CollectionEvent.objects.create(
                fish=fish,
                detection_time=detection_time,
                antenna_group_name=antenna_name,
                collection_location=collection_location
            )


if __name__ == '__main__':
    django.setup()
    from location_tracking.models import Fish, CollectionLocation, CollectionEvent

    main()
