import django
import csv
from datetime import datetime


def main():

    with open('../fixtures/fishPos_20190604.csv', mode='r') as file:
        csv_file = csv.reader(file)

        for line in list(csv_file)[20000:]:
            # print(line)

            date_time = datetime.strptime(line[0], '%m/%d/%Y %H:%M:%S.%f')
            tag_code = line[1]
            acoustic_tag_id = line[1][3:7]

            x_coordinate = line[2]
            y_coordinate = line[3]
            z_coordinate = line[4]
            try:
                fish = Fish.objects.get(acoustic_tag_id=acoustic_tag_id.lower())
                LocationEvent.objects.create(
                    date_time=date_time,
                    x_coordinate=x_coordinate,
                    y_coordinate=y_coordinate,
                    z_coordinate=z_coordinate,
                    fish=fish,
                    tag_code=tag_code
                )
            except Exception as err:
                print(f'[ERROR] {err}')
                print(f'Skipping line {line}')


if __name__ == '__main__':
    django.setup()
    from location_tracking.models import Fish, LocationEvent

    main()
