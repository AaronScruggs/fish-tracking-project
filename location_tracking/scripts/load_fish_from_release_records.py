import django
import csv
from datetime import datetime


def main():
    with open('../fixtures/release_records.csv', mode='r') as file:
        csv_file = csv.reader(file)

        for line in list(csv_file)[100:]:
            print(line)
            pit_tag_code = line[0]
            acoustic_tag_id = line[1]
            release_date = datetime.strptime(line[2], '%m/%d/%Y')
            species, _ = Species.objects.get_or_create(name=line[3])

            Fish.objects.create(
                pit_tag_code=pit_tag_code,
                acoustic_tag_id=acoustic_tag_id.lower(),
                release_date=release_date,
                species=species
            )


if __name__ == '__main__':
    django.setup()
    from location_tracking.models import Species, Fish

    main()
