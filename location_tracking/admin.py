from django.contrib import admin

from location_tracking.models import Fish, Species, LocationEvent, CollectionLocation, CollectionEvent


class CollectionEventInline(admin.TabularInline):
    model = CollectionEvent
    extra = 1


class LocationEventInline(admin.TabularInline):
    model = LocationEvent
    extra = 1


@admin.register(Fish)
class FishAdmin(admin.ModelAdmin):
    list_display = ('pit_tag_code', 'acoustic_tag_id', 'species', 'release_date')
    search_fields = ['pit_tag_code', 'acoustic_tag_id']
    inlines = (CollectionEventInline,)
    list_filter = ('species',)


@admin.register(Species)
class SpeciesAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)


@admin.register(LocationEvent)
class LocationEventAdmin(admin.ModelAdmin):
    list_display = ('date_time', 'x_coordinate', 'y_coordinate', 'z_coordinate', 'tag_code')
    search_fields = ('tag_code',)


@admin.register(CollectionLocation)
class CollectionLocationAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)


@admin.register(CollectionEvent)
class CollectionEventAdmin(admin.ModelAdmin):
    list_display = ('fish', 'detection_time', 'antenna_group_name', 'collection_location')
    search_fields = ('antenna_group_name',)
