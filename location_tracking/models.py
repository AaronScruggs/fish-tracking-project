from django.db import models


class Species(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Species'
        verbose_name_plural = 'Species'


class Fish(models.Model):
    pit_tag_code = models.CharField(max_length=255, unique=True)
    acoustic_tag_id = models.CharField(max_length=4, default='')
    species = models.ForeignKey(Species, null=True, blank=True, on_delete=models.SET_NULL)
    release_date = models.DateField()

    def __str__(self):
        return self.pit_tag_code

    @property
    def is_final_collected(self):
        return self.collectionevent_set.filter(collection_location__name='Final Collection Point').exists()

    class Meta:
        verbose_name = 'Fish'
        verbose_name_plural = 'Fish'


class LocationEvent(models.Model):
    date_time = models.DateTimeField()
    x_coordinate = models.DecimalField(max_digits=9, decimal_places=6)
    y_coordinate = models.DecimalField(max_digits=9, decimal_places=6)
    z_coordinate = models.DecimalField(max_digits=9, decimal_places=6)
    tag_code = models.CharField(max_length=255)

    fish = models.ForeignKey(Fish, on_delete=models.CASCADE)


class CollectionLocation(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class CollectionEvent(models.Model):
    fish = models.ForeignKey(Fish, on_delete=models.CASCADE)
    detection_time = models.DateTimeField()
    antenna_group_name = models.CharField(max_length=255, default='')
    collection_location = models.ForeignKey(CollectionLocation, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return f'{self.detection_time} -- {self.antenna_group_name}'
