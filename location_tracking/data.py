from typing import List

from location_tracking.models import Species, Fish


def get_species() -> Species:
    return Species.objects.all()


def get_fish_with_activity(species: Species = None) -> List[Fish]:
    if species:
        return Fish.objects.filter(species=species).prefetch_related('locationevent_set')
    return Fish.objects.all().prefetch_related('locationevent_set')
